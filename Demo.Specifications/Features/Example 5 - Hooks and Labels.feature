﻿@requires_clean_database
Feature: Hooks and Labels

	As hard as we try there are still instances where we need to add a little meta-data to our tests in 
    order to categorise them or treat them slightly differently.  This is done with @labels.

	There are times where we need to perform certain tasks before, after, or during our tests such as
    set up, or tear down test data, or configure external frameworks.  This can be achieved with hooks.

Background:

    Given the phase 1 intake has been fulfilled


@email_bob_on_success
Scenario: Candidate waiting list

	Given a candidate has donated over £100000 to my space fund
	When the candidate applies for the mission to mars
	Then they will be added to the high priority section of the waiting list
