﻿Feature: Specflow will bind to step definitions
	
	Specflow uses reflection to identify classes and methods that it needs.
	Any class marked with the BindingAttribute will be inspected by
	Specflow for potential step definitions

	The individual components of a scenario can be composed from
	many classes.

    A method can have many steps associated with it, and a step can be shared
    across many scenarios.  In fact step re-use is highly encouraged.

Scenario: Candidates below 16 years old are not eligible to be on my top secret mars mission

	Legally we're not allowed to fire children into space.

	Given a candidate is under 16
	When the candidate applies for the mission to mars
	Then they will be rejected