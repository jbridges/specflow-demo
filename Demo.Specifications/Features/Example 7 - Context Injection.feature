﻿Feature: Example 7 - Context Injection

    Managing the contexts can be fairly cumbersome and bloat your step definitions which should ideally be
    petite and beautiful.	

    Specflow has a built in IoC container that will automagically instantiate shared instances of objects and pass
    them around to the various steps.

Scenario: Sharing objects across classes using context injection

	Given I have a red pair of underpants
    And a yellow cut off tank top
    When I get down
	Then everybody goes wild
