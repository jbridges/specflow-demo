﻿Feature: Example 8 - Random Other Things

    Specflow has a fair few other features that I will cover here.


Scenario Outline: Rejigging the flux capacitor

    In this cosmic conundrum the procedure for rejigging the flux capacitor is inverted based
    on alert levels... we can use the ScopeAttribute to choose the right implementation of a
    step based on either tags, feature name or scenario title.

    Scoping can also be performed at a class level - thus allowing entire classes to be swapped in and
    out based on the scenario currently been ran.

    Specflow website also has some examples of this... including a way to ignore tests using a manual tag but
    still have the steps traced out into the report.

	Given the widget is rotated <direction>
	And the quantum spooler is <spooler state>
	When the flux capacitor is rejigged
	Then everything will be okay

@alert_level_1
Examples:
| direction | spooler state |
| clockwise | on            |

@alert_level_2
Examples:
| direction     | spooler state |
| anticlockwise | off           |



# Bindings from external sources... 
# Can be from any .NET language referenced from the feature project.
# <specFlow>
#   <stepAssemblies>
#     <stepAssembly assembly="MySharedBindings" />
#   </stepAssemblies>
# </specFlow>

Scenario: Calling steps within steps

We do this fairly often and it's great until we have to start passing tables around and then
things go downhill fast

Given there is something that is logically made up out of other steps
When I do some funky stuff
Then my steps may be more concise

# Access to the BoDi IoC container can be gained by injecting an instance of IObjectContainer into a class.
# It works very similarly to many other IoC containers.  A good use of it is for a selenium IWebDriver
