﻿Feature: Example 2 - Picking out parameters

    Specflow can pick parameters out of the text of a step by using groups within a regular expression.
    Tabular data is also paramterised.

Scenario: Candidates that have made adequate donations to my space fund will receive preferential treatment

    Rocket fuel, cocaine, and hookers, are really really expensive.  Premium backers will receive automatic
    enrollment and are eligible for a spacesuit with a gold trim and red stripes
    
	Given a candidate has donated over £100000 to my space fund
	When the candidate applies for the mission to mars
	Then they will be automatically enrolled
    And they will be entitled to a premium space suit

Scenario: Candidates will be sent a personalised welcome pack

    Considering tickets are really expensive and the candidate is risking life and limb
	it's a nice thing to send them a helpful welcome pack

    Given a candidate has applied for the mission to mars
    When the candidate is accepted onto the program
    Then they will be sent a personalised welcome pack containing
	| Item Name                           |
	| Personalised Welcome Letter         |
	| Small pack of hobnobs               |
	| Dummies guide to space flight       |
	| Voucher for last will and testament |
	| Free Pen                            |
