﻿@report_electricity_usage
Feature: Example 6 - Contexts

    Feature contexts live at the feature scope and provide some useful information as well as providing a dictionary
    to allow custom data to be shared among scenarios.  A new context is created each feature.

    Scenario context provides a similar role but is scoped to a scenario.

    An instance of a class is created once per scenario

Scenario Outline: Context usage part A

    I'm now quite bored with the space program thing... it was funny at first but trying to think
    of example gherkin about a fictional space program with dark undertones gets late fast

	Given I have a test subject called <Name>
    When the test subject is zapped with <Voltage> billion volts of electrical energy
	Then the test subject will <Outcome>
Examples:
| Name  | Voltage | Outcome   |
| Ankur | 10000   | barf      |
| Alin  | 20000   | freak out |
| Andy  | 1000000 | blow up   |


