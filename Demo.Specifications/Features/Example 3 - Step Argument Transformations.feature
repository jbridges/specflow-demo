﻿Feature: Example 3 - Step Argument Transformations

Gherkin is meant to be as natural as possible which often means the step definitions do a lot
of the heavy lifting.
To help with this we can 'stronly type' step definitions - providing automatic mapping between 
weakly typed tables and more sensible objects... and even pulling useful information out of the 
text of step definitions themselves.


Scenario: Faulty space suit rule

As a social experiment we're going to provide a faulty space suit to anybody born on an odd numbered year and having a
surname beginning with P - who applied using form 39V on a Saturday

Given a candidate has the following details
| Name               | Year of Birth |
| Fred Pinklefeather | 1975          |
And a candidate submitted form 39V on a Saturday
When they are issued a spacesuit
Then it will be faulty