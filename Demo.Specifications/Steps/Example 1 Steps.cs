﻿
using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    class MissionToMarsSteps
    {
        // Some things to note... 
        // Method names do not matter - all that matters is that the step definition
        // matches the regular expression in the attribute.
        // Also more than one attribute can be attached to a method so it may be better
        // to give the method a more logical name than simply parrotting back the regex.
        [Given(@"a candidate is under 16")]
        public void ConfigureAnUnderageCandidate()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"the candidate applies for the mission to mars")]
        public void CandidateAppliesForMarsMission()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"they will be rejected")]
        public void AssertCandidateIsRejected()
        {
            ScenarioContext.Current.Pending();
        }
    }
}