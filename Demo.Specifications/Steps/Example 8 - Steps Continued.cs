﻿using BoDi;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    public class SomethingWonderful : TechTalk.SpecFlow.Steps
    {
        [Given(@"there is something that is logically made up out of other steps")]
        public void SortOutFluxCapacitorConfiguration()
        {
            Given("the widget is rotated clockwise");
            Given("the quantum spooler is off");
        }

        [When(@"I do some funky stuff")]
        public void DoSomeFunkyStuff()
        {
        }

        [Then(@"my steps may be more concise")]
        public void AssertStepsAreMoreConcise()
        {
            Assert.Pass();
        }
    }
}
