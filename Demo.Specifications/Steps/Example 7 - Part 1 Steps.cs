﻿using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    public class GetUp
    {
        public string Top { get; set; }

        public string Bottom { get; set; }
    }

    [Binding]
    public class SetUpMySweetGetUp
    {
        private readonly GetUp _getUp;

        // Like magic, an instance of GetUp is never created.
        // SpecFlow will automatically instantiate a GetUp object when it sees it needs it then
        // share the instance through the scenario.
        public SetUpMySweetGetUp(GetUp getUp)
        {
            _getUp = getUp;
        }

        [Given(@"I have a red pair of underpants")]
        public void GivenIHaveARedPairOfUnderpants()
        {
            _getUp.Bottom = "red underpants";
        }
        
        [Given(@"a yellow cut off tank top")]
        public void GivenAYellowCutOffTankTop()
        {
            _getUp.Top = "yellow cut off tank top";
        }
    }
}