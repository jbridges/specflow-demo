﻿using System;
using System.Diagnostics;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    public class UnspecifiedContextTests
    {
        // You can have any number of hooks at each stage so rather than lumping them all
        // together it may be an idea to pull them out into smaller logical chunks that
        // each serve a particular purpose.
        [BeforeFeature("report_electricity_usage")]
        public static void InitialiseTotalVoltage()
        {
            FeatureContext.Current["electricity_usage"] = 0;
        }

        [AfterFeature("report_electricity_usage")]
        public static void ReportOnTotalVoltageUsed()
        {
            Debug.WriteLine("Electricity used: {0} gigawatts", FeatureContext.Current.Get<int>("electricity_usage"));
        }


        [Given(@"I have a test subject called (.*)")]
        public void CreateTestSubject(string name)
        {
            ScenarioContext.Current["subjectName"] = name;
        }

        [When(@"the test subject is zapped with (.*) billion volts of electrical energy")]
        public void ZapTestSubjectWithElectricity(int voltageInBillionsOfVolts)
        {
            ScenarioContext.Current["outcome"] = AdministerShock(voltageInBillionsOfVolts);

            var currentUsage = FeatureContext.Current.Get<int>("electricity_usage");
            // var currentUsage = (int)FeatureContext.Current["electricity_usage"]

            currentUsage += voltageInBillionsOfVolts;

            FeatureContext.Current["electricity_usage"] = currentUsage;
        }

        [Then(@"the test subject will (.*)")]
        public void AssertOutcome(string outcome)
        {
            Assert.That(ScenarioContext.Current.Get<string>("outcome"), Is.EqualTo(outcome));
        }

        private string AdministerShock(int voltageInBillionsOfVolts)
        {
            switch ((string)ScenarioContext.Current["subjectName"])
            {
                case "Ankur":
                    return "barf";
                case "Alin":
                    return "freak out";
                case "Andy":
                    return "blow up";
                default:
                    return "toast";
            }
        }
    }

}