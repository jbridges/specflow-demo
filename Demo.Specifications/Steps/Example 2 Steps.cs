﻿using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    class MissionToMarsExample2
    {

        // Here we're actually using the power of regular expressions to extract
        // a variable argument out of our text string and pass it into the step
        // definition.  We can do this many times in a single step.
        // In fact if I could be bothered I would also parameterize the 
        // over|under text so the step can be even more flexible.
        [Given(@"a candidate has donated over £(\d+) to my space fund")]
        public void CandidateHasDonatedOver(int donationInPounds)
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"they will be automatically enrolled")]
        public void AssertAutomaticEnrollment()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"they will be entitled to a premium space suit")]
        public void AssertPremiumSpaceSuitEntitlement()
        {
            ScenarioContext.Current.Pending();
        }


        [Given(@"a candidate has applied for the mission to mars")]
        public void CandidateHasAppliedForMission()
        {
            ScenarioContext.Current.Pending();
        }

        [When(@"the candidate is accepted onto the program")]
        public void CandidateIsAcceptedOntoProgram()
        {
            ScenarioContext.Current.Pending();
        }

        // Tables can represent generic tabular data.  They're nice BUT weekly typed and we probably
        // don't want to hunt through them in our step defintions.
        [Then(@"they will be sent a personalised welcome pack containing")]
        public void AssertWelcomePackContainsItems(Table table)
        {
            // TODO: Example table methods.
            ScenarioContext.Current.Pending();
        }


    }
}