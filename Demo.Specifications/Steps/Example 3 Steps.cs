﻿using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Demo.Specifications.Steps
{
    [Binding]
    class MissionToMarsExample3
    {
        [Given(@"a candidate has the following details")]
        public void CreateCandidateWithSpecifiedDetails(Candidate candidate)
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"a candidate submitted form 39V on a Saturday")]
        public void CreateAndSubmitCandidateForm(ApplicationForm form)
        {
            ScenarioContext.Current.Pending();
        }


        [When(@"they are issued a spacesuit")]
        public void IssueCandidateWithSpaceSuit()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"it will be faulty")]
        public void AssertSpaceSuitIsFaulty()
        {
            ScenarioContext.Current.Pending();
        }

        // Step argument transformation binding lets Specflow know that this method will transform x into y.
        // In this case a blank argument causes specflow to look at the method parameters and use this method
        // to map from Table to Candidate.
        [StepArgumentTransformation]
        public Candidate TransformCandidateFromTable(Table table)
        {
            // Table CreateInstance attempts to map columns from a table to properties on the Candidate class.
            return table.CreateInstance<Candidate>();
        }


        // Like step definitions we can match step argument transforms on regular expressions
        // and pull out groups of information.  In this case we're filling out an application form
        // from a description in a step.
        // Possibly a more practical purpose would be providing a .NET datetime from a particular
        // pattern in a step.
        [StepArgumentTransformation(@"form (\w+) on a (\w+)")]
        public ApplicationForm CreateApplicationFormFromRegex(string formName, string daySubmitted)
        {
            return new ApplicationForm {Name = formName, DaySubmitted = daySubmitted};
        }
    }

    public class Candidate
    {
        public string Name { get; set; }

        public int YearOfBirth { get; set; }
    }

    public class ApplicationForm
    {
        public string Name { get; set; }

        public string DaySubmitted { get; set; }
    }
}
