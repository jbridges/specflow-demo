﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    public class RandomSteps
    {
        private string _widgetRotationDirection = "";
        private string _quantumSpoolerState = "";
        private bool _isEverythingOkay;

        [Given(@"the widget is rotated (.*)")]
        public void WidgetIsRotated(string direction)
        {
            _widgetRotationDirection = direction;
        }

        [Given(@"the quantum spooler is (.*)")]
        public void PresetQuantumSpoolerState(string state)
        {
            _quantumSpoolerState = state;
        }

        [When(@"the flux capacitor is rejigged"), Scope(Tag="alert_level_1")]
        public void PerformAlertLevel1FluxCapacitorRejig()
        {
            _isEverythingOkay = (_widgetRotationDirection == "clockwise" && _quantumSpoolerState == "on");
        }

        [When(@"the flux capacitor is rejigged"), Scope(Tag = "alert_level_2")]
        public void PerformAlertLevel2FluxCapacitorRejig()
        {
            _isEverythingOkay = (_widgetRotationDirection == "anticlockwise" && _quantumSpoolerState == "off");
        }

        [Then(@"everything will be okay")]
        public void ThenEverythingWillBeOkay()
        {
            Assert.IsTrue(_isEverythingOkay);
        }

    }
}