using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    public class GettingDownSteps
    {
        private readonly GetUp _getUp;

        // This bad boy is a private member... I know it's only going to be used here so no
        // need to share.
        private string _everybodiesReaction = "";

        public GettingDownSteps(GetUp getUp)
        {
            _getUp = getUp;
        }

        [When(@"I get down")]
        public void WhenIGetDown()
        {
            GetDown();
        }

        [Then(@"everybody goes wild")]
        public void ThenEverybodyGoesWild()
        {
            Assert.That(_everybodiesReaction, Is.EqualTo("going wild"));
        }

        private void GetDown()
        {
            if (_getUp.Bottom == "red underpants" && _getUp.Top == "yellow cut off tank top")
            {
                _everybodiesReaction = "going wild";
            }
            else
            {
                _everybodiesReaction = "just not digging it";
            }
        }
    }
}