﻿using TechTalk.SpecFlow;

namespace Demo.Specifications.Steps
{
    [Binding]
    public class MissionToMarsExample5
    {
        // Before feature methods must be static.
        [BeforeFeature]
        public static void DoSomeCommonFeatureStuff() {}

        [BeforeFeature("requires_clean_database")]
        public static void SpinUpCleanDatabase() {}

        [BeforeScenario]
        public void DoSomeScenarioStuff() {}

        [BeforeScenario("email_bob_on_success")]
        public void SendSpamEmailToBob() {}

        // [AfterScenario]
        // [AfterFeature]
        // [BeforeTestRun]
        // [AfterTestRun]
        // [BeforeStep]
        // [AfterStep]
        // [BeforeScenarioBlock]
        // [AfterScenarioBlock]

        [Given(@"the phase 1 intake has been fulfilled")]
        public void FulfillPhase1Intake()
        {
            ScenarioContext.Current.Pending();
        }

        [Then(@"they will be added to the high priority section of the waiting list")]
        public void AssertCandidateAddedToHighPriorityWaitingList()
        {
            ScenarioContext.Current.Pending();
        }
    }
}