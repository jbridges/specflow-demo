﻿Feature: Gherkin can represent complex data
	
	In order to pass complex items to a step.
	As a level 20 Gherkineer
	I can use data tables and doc strings to get the job done

Scenario:  Tables allow us to feed sets of complex data to an individual step

	Tables allow us to add a set of test data to a step.

	Given a person has £2
	And a person adds the following items to their basket
	| Item     | Price |
	| Mars Bar | 60p   |
	| Snickers | 60p   |
	| Lion Bar | 60p   |
	| Star Bar | 60p   |
	| Topic    | 80p   |
	When they reach the checkout
	Then they will not be able to complete their purchase

Scenario: Doc strings allow us to represent large amounts of text

Given I have a big warning poster that reads
	"""
	Gherkin and the bits and bobs associated with it are such an easy to grasp, awesome, concept.
	It's so awesome that almost everybody can have a crack at it and, as such, it's often adopted by, or 
	forced upon, people without any real thought having gone into it.
	This is a lot like giving a baby a machine gun and it rarely ends well.
	Instead, like anything, it needs to be bought into by the whole team, embraced and nurtured,
	like a tiny child... a tiny child that will ultimately grow up to consume all your time and 
	ultimately ruin your life.
	"""
When people read the big warning poster
Then nobody ever listens