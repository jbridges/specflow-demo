﻿Feature: Example 2 - Gherkin 102

	In order to further educate the team on Ghekin
	As an awesome hombre
	I want to teach them all about backgrounds and tables

Background: 
	Given at least 50% of the people in the meeting are still awake
	And there are still biscuits available

Scenario: Understanding backgrounds

	Backgrounds group common preconditions for a feature.  You can think
	of them like the SetUp step in an nunit test.

	Given several scenarios with the same precondition
	When the each scenario is considered
	Then the background should be considered before each scenario in the feature