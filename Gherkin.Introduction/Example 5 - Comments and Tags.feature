﻿# is a comment... you can put it as the first character of any line
# and it will be completely ignored.

@this_tag_is_on_the_feature
Feature: Gherkin contains some things that I haven't covered yet

	In order to add additional information and meta-data to a specification
	As a concerned citizen
	I want to be shown how to do all the stuff I haven't been shown yet

@this_tag_is_on_the_scenario
Scenario: Movie ranking results in destruction

	Given I have a movie that has failed the quick pass culling algorithm
	When I execute the movie garbage collector
	Then the movie is microwaved then shot into space
	And the star will be buried alive at sea

@this_is_a_tag_on_a_scenario_outline
Scenario Outline: Movie rating quick pass culling algorithm
	Given a movie stars <actor>
	When the movie rating quick pass culling algorithm is executed
	Then the movie will <score>

@judge_has_good_taste
Examples:
	| actor          | score |
	| Hugh Grant     | Fail  |
	| Clint Eastwood | Pass  |
	
@judge_has_bad_taste
Examples:
	| actor          | score |
	| Hugh Grant     | Pass  |
	| Clint Eastwood | Fail  |