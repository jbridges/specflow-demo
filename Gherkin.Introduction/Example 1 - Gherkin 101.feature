﻿Feature: Example 1 - Basic Gherkin

	In order that I can convey requirements in a universally understood way
	As somebody who cares about requirements
	I want to write some Gherkin

Scenario: Exploring a Gherkin scenario

	Gherkin follows the now familiar given, when, then pattern.
	Each line is called a step and we call the whole thing a specification because
	it specifies some behaviour we care about.

	Given some preconditions
	When the state of the system is modified
	Then there will be an observable result

Scenario: A more complex scenario

	You might want many pre-conditions (givens) and you may have several observable results (thens).
	You could even change a lot of state (whens).	

	Given the widget has been rotated clockwise
	Given the capacitor is fully charged
	When the machine is switched to spooky mode
	When the lever is pulled
	Then the dead will roam the earth
	Then the effect will only be temporary


Scenario: Using better language

	Ands and Buts are availble but are syntatic sugar having the effect of the 
	current scenario block they are part of. 
	
	Given the widget has been rotated clockwise
	And the capacitor is fully charged
	When the machine is switched to spooky mode
	And the lever is pulled
	Then the dead will roam the earth
	But the effect will only be temporary