﻿Feature: Gherkin can run the same specification multiple times with different data

	In order to throroughly test somethings behaviour
	As somebody with too much time on their hands
	I want to run the same test multiple times with different sets of data

	
Scenario Outline: Specifying complex behaviour with different imputs

	Scenario outlines let us provide a template of a specification.
	On each iteration of this specification the placeholder text will be replaced with the
	data from a specific row.

	Given a <projectile> is fired at <speed> kilometers per hour
	And <name> is wearing a <protective_garment>
	When <name> is hit by the <projectile>
	Then then they will be <consequence>


	Examples:

	| name  | protective_garment   | projectile  | speed | consequence    |
	| Ankur | string vest          | cannon ball | 1250  | mildly annoyed |
	| Seb   | reactive tank armour | omelette    | 50    | obliterated    |
	| James | mankini              | anvil       | 90    | knocked over   |